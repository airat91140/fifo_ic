/****************************************************************************
 * fifo_write_seq.sv
 ****************************************************************************/
`ifndef FIFO_WRITE_SEQ__SV
`define FIFO_WRITE_SEQ__SV

`include "simple_item.sv"

/**
 * Class: fifo_write_seq
 *
 * sequence class with writing to full fifo
 */
class fifo_write_seq #(parameter P_WIDTH = 8) extends uvm_sequence #(simple_item#(P_WIDTH));
    rand integer count;
    fifo_agent_cfg agt_cfg;

    constraint count_c {
        count inside {[9500 : 10000]};
    }

    function new(string name = "fifo_write_seq");
        super.new(name);
    endfunction : new

    `uvm_object_param_utils(fifo_ic_tb_pkg::fifo_write_seq#(P_WIDTH))

    virtual function void set_config(fifo_agent_cfg agt_cfg);
        this.agt_cfg = agt_cfg;
    endfunction : set_config

    virtual task body();
        repeat (count) begin
            req = simple_item#(P_WIDTH)::type_id::create("write_item");
            req.read_dist_c.constraint_mode(0);
            req.write_dist_c.constraint_mode(1);
            `uvm_rand_send(req)
        end
        req = simple_item#(P_WIDTH)::type_id::create("item");
        req.is_write = 1'b0;
        `uvm_send(req)
        if (agt_cfg.sequence_log) begin
                `uvm_info("item sent ",
                    $sformatf("WR: %b, RD: %b, \ndata: %b",
                        req.is_write, req.is_read, req.data), UVM_MEDIUM)
        end
    endtask : body

endclass : fifo_write_seq

`endif // FIFO_WRITE_SEQ__SV
