`ifndef FIFO_TOP__SV
`define FIFO_TOP__SV

`timescale 1ns/1ns
`define P_WIDTH 8
`define P_DEPTH 8
`define P_THOLD 4

import uvm_pkg::*;
`include "uvm_macros.svh"

`include "fifo_ic_tb_pkg.sv"
import fifo_ic_tb_pkg::*;

`include "test_lib.sv"

module top;
    parameter P_DEPTH = `P_DEPTH;
    parameter P_THOLD = `P_THOLD;
    parameter P_WIDTH = `P_WIDTH;
    
    logic clk100M, clk50M, clk71M, rd_clk, wr_clk;
    logic rst;

    `ifdef RD_CLK
        assign rd_clk = `RD_CLK;
    `else
        assign rd_clk = clk100M;
    `endif

    `ifdef WR_CLK
        assign wr_clk = `WR_CLK;
    `else
        assign wr_clk = ~clk100M;
    `endif

    fifo_ic_inf #(
        .P_DEPTH(P_DEPTH),
        .P_WIDTH(P_WIDTH)
    ) inf (
        .rd_clk_i(rd_clk),
        .wr_clk_i(wr_clk),
        .wr_rst_n_i(rst),
        .rd_rst_n_i(rst)
    );

    virtual fifo_ic_inf #(
        .P_DEPTH(P_DEPTH),
        .P_WIDTH(P_WIDTH)
    ) vif;

    initial begin
        clk100M = 0;
        clk50M = 0;
        clk71M = 0;
    end

    initial begin
        rst = 1;
        #5ns rst = 0;
        fork
            @(posedge wr_clk);
            @(posedge rd_clk);
        join
        rst = 1;
    end

    initial begin
        fork
            forever #10ns clk50M = ~clk50M;
            forever #5ns clk100M = ~clk100M;
            forever #7ns clk71M = ~clk71M;
        join_none
    end

    initial begin
        uvm_root root;
        vif = inf;
        root = uvm_root::get();
        uvm_config_db#(virtual fifo_ic_inf#(.P_DEPTH(P_DEPTH), .P_WIDTH(P_WIDTH)))::set(root, "*", "vif_test", vif);
        uvm_config_db#(virtual fifo_ic_inf#(.P_DEPTH(P_DEPTH), .P_WIDTH(P_WIDTH)))::set(root, "*", "vif_monitor", vif);
        run_test();
    end

    fifo_ic #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ) i_fifo_ic (inf.DUT);

endmodule : top

`endif // FIFO_TOP__SV

