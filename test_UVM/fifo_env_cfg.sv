`ifndef FIFO_ENV_CFG__SV
`define FIFO_ENV_CFG__SV

class fifo_env_cfg extends uvm_object;
    `uvm_object_utils(fifo_ic_tb_pkg::fifo_env_cfg)

    fifo_agent_cfg agt_cfg;
    bit scoreboard_log;

    function new(string name = "fifo_env_cfg");
        super.new(name);
        scoreboard_log = 1;
        agt_cfg = fifo_agent_cfg::type_id::create("fifo_agent_config");
    endfunction : new
endclass : fifo_env_cfg

`endif // FIFO_ENV_CFG__SV
