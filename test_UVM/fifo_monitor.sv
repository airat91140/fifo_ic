/****************************************************************************
 * fifo_monitor.sv
 ****************************************************************************/
`ifndef FIFO_MONITOR__SV
`define FIFO_MONITOR__SV

`include "simple_item.sv"
`include "fifo_transfer.sv"

/**
 * Class: fifo_monitor
 *
 * monitor class for accessing outputs of the DUT
 */
class fifo_monitor #(
    parameter P_WIDTH = 8,
    parameter P_THOLD = 4,
    parameter P_DEPTH = 8
) extends uvm_monitor;

    fifo_agent_cfg agt_cfg;

    virtual fifo_ic_inf #(
        .P_DEPTH(P_DEPTH),
        .P_WIDTH(P_WIDTH)
    ).MONITOR vif;

    uvm_analysis_port #(fifo_transfer#(P_WIDTH)) item_collected_port;

    fifo_transfer #(P_WIDTH) trans;
    
    covergroup cg;
        full_wr_front: coverpoint {trans.read, trans.write, trans.full, trans.wr_front};
        empty_wr_front: coverpoint {trans.read, trans.write, trans.empty, trans.wr_front};
        afull_wr_front: coverpoint {trans.read, trans.write, trans.afull, trans.wr_front};
        aempty_wr_front: coverpoint {trans.read, trans.write, trans.aempty, trans.wr_front};
        wr_thold_wr_front: coverpoint {trans.read, trans.write, trans.wr_thold, trans.wr_front};
        rd_thold_wr_front: coverpoint {trans.read, trans.write, trans.rd_thold, trans.wr_front};
        full_rd_front: coverpoint {trans.read, trans.write, trans.full, trans.rd_front};
        empty_rd_front: coverpoint {trans.read, trans.write, trans.empty, trans.rd_front};
        afull_rd_front: coverpoint {trans.read, trans.write, trans.afull, trans.rd_front};
        aempty_rd_front: coverpoint {trans.read, trans.write, trans.aempty, trans.rd_front};
        wr_thold_rd_front: coverpoint {trans.read, trans.write, trans.wr_thold, trans.rd_front};
        rd_thold_rd_front: coverpoint {trans.read, trans.write, trans.rd_thold, trans.rd_front};
    endgroup

    `uvm_component_param_utils(fifo_ic_tb_pkg::fifo_monitor#(.P_WIDTH(P_WIDTH), .P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD)))

    function new(string name, uvm_component parent);
        super.new(name, parent);
        cg = new();
        item_collected_port = new("item_collected_port", this);
    endfunction : new

    virtual function void set_config(fifo_agent_cfg agt_cfg);
        this.agt_cfg = agt_cfg;
    endfunction : set_config

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!uvm_config_db#(virtual fifo_ic_inf#(.P_DEPTH(P_DEPTH),
                                                .P_WIDTH(P_WIDTH)))::get(this, "", "vif_monitor", vif))
            `uvm_fatal("NOVIF", {"virtual interface must be set for: ", get_full_name(),".vif_monitor"});
    endfunction : build_phase

    virtual task run_phase(uvm_phase phase);
        collect_transactions();
    endtask : run_phase

    virtual protected task collect_transactions();
        bit wr_front, rd_front;
        fork
            @(posedge vif.wr_rst_n_i);
            @(posedge vif.rd_rst_n_i);
        join
        forever begin
            wr_front = 0;
            rd_front = 0;
            fork // when rd and  wr clocks act simultaneously both wr_front and rd front are set to 1. However, when there is only one posedge only one becomes 1.
                begin
                    @(posedge vif.wr_clk_i);
                    wr_front = 1;
                end
                begin
                    @(posedge vif.rd_clk_i);
                    rd_front = 1;
                end
            join_any
            if (no_operation(wr_front, rd_front) && !agt_cfg.coverage_on) begin
                disable fork;
                continue;
            end
            trans = fifo_transfer#(P_WIDTH)::type_id::create("fifo_transfer");
            write_to_trans();
            #1ps;
            disable fork;
            trans.wr_front = wr_front;
            trans.rd_front = rd_front;
            if (agt_cfg.coverage_on)
                perform_transfer_coverage();
            if (no_operation(wr_front, rd_front)) begin
                disable fork;
                continue;
            end
            item_collected_port.write(trans);
            if (agt_cfg.monitor_log) begin
                `uvm_info("trans sent",
                    $sformatf("WR: %b, RD: %b \nfull: %b, afull: %b, empty: %b, aempty: %b \ndata_i: %b, data_o: %b",
                        trans.write, trans.read, trans.full, trans.afull, trans.empty, trans.aempty, trans.data_i, trans.data_o), UVM_MEDIUM)
            end
        end
    endtask : collect_transactions

    function bit no_operation(bit wr_front, bit rd_front);
        return (!vif.rd_en_i && !vif.wr_en_i) || 
               (rd_front && !wr_front && !vif.rd_en_i) || 
               (wr_front && !rd_front && !vif.wr_en_i);
    endfunction : no_operation

    protected task write_to_trans();
        trans.write    = vif.wr_en_i;
        trans.read     = vif.rd_en_i;
        trans.data_i   = vif.wr_data_i;
        trans.data_o   = vif.rd_data_o;
        trans.empty    = vif.rd_empty_o;
        trans.aempty   = vif.rd_aempty_o;
        trans.full     = vif.wr_full_o;
        trans.afull    = vif.wr_afull_o;
        trans.wr_thold = vif.wr_thold_o;
        trans.rd_thold = vif.rd_thold_o;
    endtask : write_to_trans

    virtual protected function void perform_transfer_coverage();
        cg.sample();
    endfunction : perform_transfer_coverage

endclass : fifo_monitor

`endif // FIFO_MONITOR__SV
