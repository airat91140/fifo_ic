/****************************************************************************
 * simple_item.sv
 ****************************************************************************/
`ifndef SIMPLE_ITEM__SV
`define SIMPLE_ITEM__SV

import uvm_pkg::*;
`include "uvm_macros.svh"

/**
 * Class: simple_item
 *
 * class that provides implementation of transaction from sequencer to driver
 */
class simple_item #(parameter P_WIDTH = 8) extends uvm_sequence_item;
    rand bit is_write, is_read;
    rand bit [P_WIDTH - 1 : 0] data;

    `uvm_object_param_utils(fifo_ic_tb_pkg::simple_item#(P_WIDTH))

    constraint read_dist_c {
        is_write == 0;
        is_read     dist {0:=50, 1:=50};
    }

    constraint write_dist_c {
        is_write    dist {0:=50, 1:=50};
        is_read == 0;
    }

    function new(string name = "simple_item");
        super.new(name);
    endfunction : new

endclass : simple_item

`endif // SIMPLE_ITEM__SV
