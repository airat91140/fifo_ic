`ifndef FIFO_IC_TB_PKG__SV
`define FIFO_IC_TB_PKG__SV

package fifo_ic_tb_pkg;
  
import uvm_pkg::*;
`include "uvm_macros.svh"

`include "fifo_agent_cfg.sv"
`include "fifo_env_cfg.sv"
`include "fifo_env.sv"
`include "fifo_read_seq.sv"
`include "fifo_write_seq.sv"

endpackage : fifo_ic_tb_pkg

`endif // FIFO_IC_TB_PKG__SV
