/****************************************************************************
 * fifo_agent.sv
 ****************************************************************************/
`ifndef FIFO_AGENT__SV
`define FIFO_AGENT__SV

`include "top.sv"
`include "fifo_read_driver.sv"
`include "fifo_write_driver.sv"
`include "fifo_monitor.sv"
`include "simple_item.sv"

/**
 * Class: fifo_agent
 *
 * class for implementing agent functions
 */
class fifo_agent #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8,
    parameter P_THOLD = 4
) extends uvm_agent;

    fifo_agent_cfg cfg;

    `uvm_component_param_utils(fifo_ic_tb_pkg::fifo_agent#(P_WIDTH, P_DEPTH, P_THOLD))

    uvm_sequencer #(simple_item#(P_WIDTH)) read_sequencer;

    fifo_read_driver #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ) read_driver;

    uvm_sequencer #(simple_item#(P_WIDTH)) write_sequencer;

    fifo_write_driver #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ) write_driver;

    fifo_monitor #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ) monitor;

    virtual function void set_config(fifo_agent_cfg cfg);
        this.cfg = cfg;
    endfunction
    
    function new(string name = "fifo_agent", uvm_component parent);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        monitor = fifo_monitor #(
            .P_DEPTH(P_DEPTH), 
            .P_THOLD(P_THOLD), 
            .P_WIDTH(P_WIDTH)
        )::type_id::create("monitor", this);
        monitor.set_config(cfg);
        if (cfg.active == UVM_ACTIVE) begin
            read_sequencer = uvm_sequencer#(simple_item#(P_WIDTH))::type_id::create("read_sequencer", this);
            write_sequencer = uvm_sequencer#(simple_item#(P_WIDTH))::type_id::create("write_sequencer", this);
            read_driver = fifo_read_driver#(.P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD), .P_WIDTH(P_WIDTH))::type_id::create("read_driver", this);
            read_driver.set_config(cfg);
            write_driver = fifo_write_driver#(.P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD), .P_WIDTH(P_WIDTH))::type_id::create("write_driver", this);
            write_driver.set_config(cfg);
        end
    endfunction : build_phase

    virtual function void connect_phase(uvm_phase phase);
        if (cfg.active == UVM_ACTIVE) begin
            read_driver.seq_item_port.connect(read_sequencer.seq_item_export);
            write_driver.seq_item_port.connect(write_sequencer.seq_item_export);
        end
    endfunction : connect_phase

endclass : fifo_agent

`endif  // FIFO_AGENT__SV
