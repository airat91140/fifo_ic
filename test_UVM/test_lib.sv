/****************************************************************************
 * test_lib.sv
 ****************************************************************************/
`ifndef TEST_LIB__SV
`define TEST_LIB__SV

`include "fifo_base_test.sv"
`include "fifo_read_seq.sv"
`include "fifo_write_seq.sv"

/**
 * Class: multiple_operations_with_overflow_test
 *
 * test with multiple operations per one cycle with testing the possibility to write into full or read from empty
 */
class multiple_operations_with_overflow_test extends fifo_base_test;

    fifo_read_seq#(super.P_WIDTH) read_seq;
    fifo_write_seq#(super.P_WIDTH) write_seq;

    `uvm_component_utils(multiple_operations_with_overflow_test)

    function new(string name = "multiple_operations_with_overflow_test", uvm_component parent = null);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        read_seq = fifo_read_seq#(super.P_WIDTH)::type_id::create("rd_seq");
        write_seq = fifo_write_seq#(super.P_WIDTH)::type_id::create("wr_seq");
        read_seq.randomize();
        write_seq.randomize();
        super.build_phase(phase);
        read_seq.set_config(super.env_cfg.agt_cfg);
        write_seq.set_config(super.env_cfg.agt_cfg);
    endfunction : build_phase

    virtual task start();
        fork
            read_seq.start(env.agent.read_sequencer);
            write_seq.start(env.agent.write_sequencer);
        join
    endtask : start
    
endclass : multiple_operations_with_overflow_test

`endif  //  TEST_LIB__SV
