/****************************************************************************
 * fifo_read_seq.sv
 ****************************************************************************/
`ifndef FIFO_READ_SEQ__SV
`define FIFO_READ_SEQ__SV

`include "simple_item.sv"

/**
 * Class: fifo_read_seq
 *
 * sequence class with reading from empty
 */
class fifo_read_seq #(parameter P_WIDTH = 8) extends uvm_sequence #(simple_item#(P_WIDTH));
    rand integer count;
    simple_item #(P_WIDTH) req;
    fifo_agent_cfg agt_cfg;

    constraint count_c {
        count inside {[9500 : 10000]};
    }

    `uvm_object_param_utils(fifo_ic_tb_pkg::fifo_read_seq#(P_WIDTH))


    function new(string name = "fifo_read_seq");
        super.new(name);
    endfunction : new

    virtual function void set_config(fifo_agent_cfg agt_cfg);
        this.agt_cfg = agt_cfg;
    endfunction : set_config

    virtual task body();
        repeat (count) begin
            req = simple_item#(P_WIDTH)::type_id::create("item");
            req.read_dist_c.constraint_mode(1);
            req.write_dist_c.constraint_mode(0);
            `uvm_rand_send(req)
            if (agt_cfg.sequence_log) begin
                `uvm_info("item sent ",
                    $sformatf("WR: %b, RD: %b\ndata: %b",
                        req.is_write, req.is_read, req.data), UVM_MEDIUM)
            end
        end
        req = simple_item#(P_WIDTH)::type_id::create("item");
        req.is_read = 1'b0;
        `uvm_send(req)
    endtask : body

endclass : fifo_read_seq

`endif // FIFO_READ_SEQ__SV
