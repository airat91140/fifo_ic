/****************************************************************************
 * fifo_read_driver.sv
 ****************************************************************************/
`ifndef FIFO_READ_DRIVER__SV
`define FIFO_READ_DRIVER__SV

`include "simple_item.sv"

/**
 * Class: fifo_read_driver
 *
 * driver class for reading from FIFO
 */
class fifo_read_driver #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8,
    parameter P_THOLD = 4
) extends uvm_driver #(simple_item#(P_WIDTH));

    simple_item#(P_WIDTH) item;
    fifo_agent_cfg agt_cfg;

    virtual fifo_ic_inf #(
        .P_DEPTH(P_DEPTH),
        .P_WIDTH(P_WIDTH)
    ).TEST vif;

    `uvm_component_param_utils(fifo_ic_tb_pkg::fifo_read_driver#(P_WIDTH, P_DEPTH, P_THOLD))

    function new(string name = "fifo_read_driver", uvm_component parent);
        super.new(name, parent);
    endfunction : new

    virtual function void set_config(fifo_agent_cfg agt_cfg);
        this.agt_cfg = agt_cfg;
    endfunction : set_config

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!uvm_config_db#(virtual fifo_ic_inf#(.P_DEPTH(P_DEPTH),
                                                .P_WIDTH(P_WIDTH)))::get(this, "", "vif_test", vif))
            `uvm_fatal("NOVIF", {"virtual interface must be set for: ", get_full_name(),".vif_test"});
    endfunction : build_phase

    task run_phase(uvm_phase phase);
        vif.rd_en_i = 1'b0;
        fork
            @(posedge vif.wr_rst_n_i);
            @(posedge vif.rd_rst_n_i);
        join
        forever begin
            @(posedge vif.rd_clk_i);
            seq_item_port.get_next_item(item);
            drive_item(item);
            if (agt_cfg.driver_log) begin
                `uvm_info("item is driven",
                    $sformatf("wr_en_i: %b, rd_en_i: %b \nfull_o: %b, afull_o: %b, empty_o: %b, aempty_o: %b \ndata_i: %b, data_o: %b",
                vif.wr_en_i, vif.rd_en_i, vif.wr_full_o, vif.wr_afull_o, vif.rd_empty_o, vif.rd_aempty_o, vif.rd_data_o, vif.wr_data_i), UVM_MEDIUM)
            end
            seq_item_port.item_done();
        end
    endtask : run_phase

    task drive_item(simple_item#(P_WIDTH) item);
        vif.rd_en_i = item.is_read;
    endtask : drive_item

endclass : fifo_read_driver

`endif // FIFO_READ_DRIVER__SV
