# FIFO с независимым тактированием (lab2)

## Структура проекта:
* [RTL](./RTL) - RTL-код проекта
    * [counter.sv](./RTL/counter.sv) - счетчик в коде грея
    * [fifo_ic.sv](./RTL/fifo_ic.sv) - модуль верхнего уровня
    * [fifo_ic_inf.sv](./RTL/fifo_ic_inf.sv) - интерфейс модуля верхнего уровня
    * [syncmem_dp.sv](./RTL/syncmem_dp.sv) - двухпортовая память
* [sim](./sim) - скрипты для симуляции
    * [clean.do](./sim/clean.do) - очистка файлов компиляции
    * [compile.do](./sim/compile.do) - компиляции файлов проекта
    * [param.do](./sim/param.do) - параметры компиляции
    * [rerun.do](./sim/rerun.do) - компиляции и запуск симулятора
    * [sim.do](./sim/sim.do) - запуск симуляции проекта
    * [wave.do](./sim/wave.do) - открытие временной диаграммы FIFO
* [test_UVM](./test_UVM) - тестировние проекта с использованием UVM
    * [fifo_agent.sv](./test_UVM/fifo_agent.sv) - класс агента
    * [fifo_write_driver.sv](./test_UVM/fifo_write_driver.sv) - класс драйвера для записи значений
    * [fifo_write_driver.sv](./test_UVM/fifo_read_driver.sv) - класс драйвера для чтения
    * [fifo_env.sv](./test_UVM/fifo_env.sv) - класс верхнего environment
    * [fifo_monitor.sv](./test_UVM/fifo_monitor.sv) - класс монитора
    * [fifo_scoreboard.sv](./test_UVM/fifo_scoreboard.sv) - класс scoreboard
    * [fifo_base_test.sv](./test_UVM/fifo_base_test.sv) - базовый класс для тестов (расширение uvm_test)
    * [fifo_write_seq.sv](./test_UVM/fifo_write_seq.sv) - класс sequence для генерации последовательности транзакций записи
    * [fifo_read_seq.sv](./test_UVM/fifo_read_seq.sv) - класс sequence для генерации последовательности транзакций чтения
    * [fifo_transfer.sv](./test_UVM/fifo_transfer.sv) - объект, передаваемый от монитора в scoreboard. Содержит в себе сигналы, снятые по фронту
    * [simple_item.sv](./test_UVM/simple_item.sv) - класс объекта, передаваемый от sequencer к драйверу
    * [fifo_tb_cfg.sv](./test_UVM/fifo_tb_cfg.sv) - класс конфигурации проекта
    * [test_lib.sv](./test_UVM/test_lib.sv) - содержит в себе один crt тест с возможностью переполнения и переопустошения fifo
    * [top.sv](./test_UVM/top.sv) - верхний модуль проекта

## Пояснения к запуску

Чтобы выбрать запускаемый тест надо в [param.do#L5](./sim/param.do#L5) поменять директорию проекта на ту где лежит проект.
Чтобы выбрать режимы работы тактовых генераторов для чтения и записи надо в [param.do#L17](./sim/param.do#L17) поменять read_clock и write_clock на один из представленных выше.
Для запуска проекта надо выполнить последовательность скриптов:
```
do clean.do
do compile.do
do sim.do
do wave.do
```
Более быстрый способ:
```
do rerun.do
```

Для включения логов надо в файле [fifo_tb_cfg.sv](./test_UVM/fifo_tb_cfg.sv) настроить объекты, в которых интересуют логи. Для включения покрытия - надо в [fifo_tb_cfg.sv#L9](./test_UVM/fifo_tb_cfg.sv#L9) поменять флаг покрытия и в [param.do#L64](./sim/param.do#L64) установить флаг в 1.

## Пояснения к проекту
Данный проект содержит в себе реализацию FIFO с независимым тактированием. Для тестирования был реализован crt тест (передача до двух операций одновременно с проверкой переполнения и переопустошения)
