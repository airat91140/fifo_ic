coverage save -cvg ../ucdb/tmp.ucdb

############################################# 
## Merge all coverage data to total
#############################################
vcover merge ../ucdb/total.ucdb ../ucdb/*.ucdb

############################################# 
## Show total coverage report
#############################################
coverage open ../ucdb/total.ucdb

