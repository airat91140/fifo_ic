############################################# 
## Create structural coverage report
#############################################
coverage report -file scover_report.txt -byinstance -assert -directive -cvg -code {bcsf}

############################################# 
## Show structural coverage report
#############################################
notepad scover_report.txt
