#do clean.do

source param.do

vlib work

if {$code_coverage} {
    quietly set cov_param "+cover=bcsf"
} else {
    quietly set cov_param "+nocover"
}

#compile DUT
vlog -sv -timescale $ts_param $cov_param -f $rtl_dir/source.list +libext+.v+.vlib 

#compile TB environment
vlog -timescale $ts_param -L mtiUvm +define+RD_CLK=$read_clock $cov_param +define+WR_CLK=$write_clock +incdir+$tb_dir+$proj_home -f $tb_dir/tb.list

