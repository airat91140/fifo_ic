module fifo_ic #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8,
    parameter P_THOLD = 4
) (
    fifo_ic_inf.DUT inf
);

    logic [$clog2(P_DEPTH) : 0] rd_ptr, wr_ptr, next_rd_ptr, next_wr_ptr;
    logic [$clog2(P_DEPTH) : 0] bin_rd_ptr, bin_wr_ptr, bin_next_wr_ptr, bin_next_rd_ptr;

    syncmem_dp #(
        .P_DEPTH(P_DEPTH + P_DEPTH),
        .P_WIDTH(P_WIDTH)
    ) mem (
        .data_i(inf.wr_data_i),
        .wr_en(inf.wr_en_i & ~inf.wr_full_o),
        .wr_addr(wr_ptr),
        .rd_addr(rd_ptr),
        .clk(inf.wr_clk_i),
        .rst(~inf.rd_rst_n_i | ~inf.wr_rst_n_i),
        .data_out(inf.rd_data_o)
    );

    counter #(
        .N(P_DEPTH)
    ) wr_counter (
        .clock(inf.wr_clk_i),
        .rst(~inf.wr_rst_n_i),
        .en(inf.wr_en_i & ~inf.wr_full_o),
        .q(wr_ptr),
        .next_q(next_wr_ptr),
        .bin_q(bin_wr_ptr),
        .bin_next_q(bin_next_wr_ptr)
    );

    counter #(
        .N(P_DEPTH)
    ) rd_counter (
        .clock(inf.rd_clk_i),
        .rst(~inf.rd_rst_n_i),
        .en(inf.rd_en_i & ~inf.rd_empty_o),
        .q(rd_ptr),
        .next_q(next_rd_ptr),
        .bin_q(bin_rd_ptr),
        .bin_next_q(bin_next_rd_ptr)
    );

    assign inf.rd_empty_o = rd_ptr == wr_ptr;
    assign inf.wr_full_o = (bin_rd_ptr[$clog2(P_DEPTH) - 1 : 0] == bin_wr_ptr[$clog2(P_DEPTH) - 1 : 0]) & (bin_rd_ptr[$clog2(P_DEPTH)] != bin_wr_ptr[$clog2(P_DEPTH)]);
    assign inf.rd_aempty_o = next_rd_ptr == wr_ptr;
    assign inf.wr_afull_o = (bin_rd_ptr[$clog2(P_DEPTH) - 1 : 0] == bin_next_wr_ptr[$clog2(P_DEPTH) - 1 : 0]) & (bin_rd_ptr[$clog2(P_DEPTH)] != bin_next_wr_ptr[$clog2(P_DEPTH)]);
    assign inf.wr_thold_o = bin_wr_ptr > bin_rd_ptr ? (bin_wr_ptr - bin_rd_ptr) >= P_THOLD            :
                            bin_rd_ptr > bin_wr_ptr ? (P_DEPTH + P_DEPTH - bin_rd_ptr + bin_wr_ptr) >= P_THOLD  :
                            inf.wr_full_o;
    assign inf.rd_thold_o = bin_wr_ptr > bin_rd_ptr ? (bin_wr_ptr - bin_rd_ptr) <= P_THOLD            :
                            bin_rd_ptr > bin_wr_ptr ? (P_DEPTH + P_DEPTH - bin_rd_ptr + bin_wr_ptr) <= P_THOLD  :
                            inf.rd_empty_o;

endmodule : fifo_ic
