module counter #(
    parameter N = 8
) (
    input                        clock,
    input                        rst,
    input                        en,
    output logic [$clog2(N) : 0] q, next_q, // 1 additional msb for fullness/emptyness recognition
    output logic [$clog2(N) : 0] bin_q, bin_next_q
);

    logic [$clog2(N): 0] gray_next_next_state, bin_next_next_state;

    always_comb begin
        for (int i = 0; i <= $clog2(N); ++i) begin
            bin_next_q[i] = ^(next_q >> i);
        end
        bin_next_next_state = bin_next_q + 1;
        bin_q = bin_next_q - 1;
        gray_next_next_state = (bin_next_next_state >> 1) ^ bin_next_next_state;
    end

    always @(posedge clock) begin
        if (rst) begin
            q = 0;
            next_q = 1;
        end
        else if (en) begin
            q = next_q;
            next_q = gray_next_next_state;
        end
    end

endmodule : counter
