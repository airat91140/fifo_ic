module syncmem_dp #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8
) (
    input  [P_WIDTH - 1 : 0]         data_i, // data to write in mem
    input                            wr_en, // enable writing into the mem
    input  [$clog2(P_DEPTH) - 1 : 0] wr_addr, // address to write
    input  [$clog2(P_DEPTH) - 1 : 0] rd_addr, // address to read
    input                            clk,
    input                            rst,
    output [P_WIDTH - 1 : 0]         data_out   // data at the rd_addr        
);

    localparam AD_P_WIDTH = $clog2(P_DEPTH);

    reg [P_WIDTH - 1 : 0] mem [P_DEPTH - 1 : 0];

    assign data_out = mem[rd_addr];
   
    always @(posedge clk) begin
        if (rst)
            mem <= '{P_DEPTH{0}};
        else if (wr_en)
            mem[wr_addr] <= data_i;
    end

endmodule : syncmem_dp
