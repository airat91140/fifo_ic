interface fifo_ic_inf #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8
) (
    input  wr_clk_i,
    input  rd_clk_i,
    input  wr_rst_n_i,
    input  rd_rst_n_i);

    logic [P_WIDTH - 1 : 0] wr_data_i;
    logic                   wr_en_i;
    logic                   wr_full_o;
    logic                   wr_afull_o;
    logic                   wr_thold_o;
    logic [P_WIDTH - 1 : 0] rd_data_o;
    logic                   rd_en_i;
    logic                   rd_empty_o;
    logic                   rd_aempty_o;
    logic                   rd_thold_o;

modport DUT (
    input  wr_data_i,
    input  wr_en_i,
    output wr_full_o,
    output wr_afull_o,
    output wr_thold_o,
    output rd_data_o,
    input  rd_en_i,
    output rd_empty_o,
    output rd_aempty_o,
    output rd_thold_o,
    input  wr_clk_i,
    input  rd_clk_i,
    input  wr_rst_n_i,
    input  rd_rst_n_i
);

modport TEST (
    output wr_data_i,
    output wr_en_i,
    input  wr_full_o,
    input  wr_afull_o,
    input  wr_thold_o,
    input  rd_data_o,
    output rd_en_i,
    input  rd_empty_o,
    input  rd_aempty_o,
    input  rd_thold_o,
    input  wr_clk_i,
    input  rd_clk_i,
    input  wr_rst_n_i,
    input  rd_rst_n_i
);

modport MONITOR (
    input  wr_data_i,
    input  wr_en_i,
    input  wr_full_o,
    input  wr_afull_o,
    input  wr_thold_o,
    input  rd_data_o,
    input  rd_en_i,
    input  rd_empty_o,
    input  rd_aempty_o,
    input  rd_thold_o,
    input  wr_clk_i,
    input  rd_clk_i,
    input  wr_rst_n_i,
    input  rd_rst_n_i
);

endinterface : fifo_ic_inf
